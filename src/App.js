import React from 'react';
import {Admin, Layout, Resource, AppBar} from 'react-admin';
import dataProvider from './dataProvider';
import authProvider from './authProvider';
import {EmployeeList} from './list/employeeList';
import {EmployeeCreate, EmployeeEdit, EmployeeShow} from './crud/employeeCrud';
import customRoutes from './customRoute';
import {Permission, permissionHandler} from './utils';
import {EmployeeAttendanceList} from './list/employeeAttendanceList';
import {EmployeeAttendanceCreate, EmployeeAttendanceEdit, EmployeeAttendanceShow} from './crud/employeeAttendanceCrud';
import {BenefitCreate, BenefitEdit} from './crud/benefitCrud';
import {BenefitList} from './list/benefitList';
import {EmployeeBenefitList} from './list/hrEmployeeBenefitList';
import {EmployeeBenefitEdit} from './crud/employeeBenefitCrud';
import {Menu, CustomUserMenu} from './menu';
import {createMuiTheme} from '@material-ui/core';
import {blue, indigo} from '@material-ui/core/colors';
import {LocalHospital, MonetizationOn, People, Schedule} from '@material-ui/icons';

const theme = createMuiTheme({
  palette: {
    type: 'light',
    primary: blue,
    secondary: indigo,
  },
});

function App() {
  return (
      <Admin authProvider={authProvider} theme={theme} layout={CustomLayout} customRoutes={customRoutes} dataProvider={dataProvider} title="HeroTool">
        {
          permissions => {
            return [
              /* Employee page minimum authority required Permission.MANAGEMENT */
              permissionHandler(permissions, Permission.MANAGEMENT) ?
                  <Resource name="employees"
                            list={EmployeeList}
                            create={EmployeeCreate}
                            show={EmployeeShow}
                            edit={EmployeeEdit}
                            icon={People}
                            options={{label: 'Employees'}}/> :
                  null,
              permissionHandler(permissions, Permission.HR) ?
                  <Resource name="benefits"
                            list={BenefitList}
                            create={BenefitCreate}
                            edit={BenefitEdit}
                            icon={LocalHospital}
                            options={{label: 'Benefits'}}/> :
                  null,
              permissionHandler(permissions, Permission.HR) ?
                  <Resource name="employee-benefits"
                            list={EmployeeBenefitList}
                            edit={EmployeeBenefitEdit}
                            icon={MonetizationOn}
                            options={{label: 'Salary & Benefit'}}/> :
                  null,
              <Resource name="attendance"
                        list={EmployeeAttendanceList}
                        create={EmployeeAttendanceCreate}
                        edit={EmployeeAttendanceEdit}
                        show={EmployeeAttendanceShow}
                        icon={Schedule}
                        options={{label: 'Attendance'}}/>,
            ];
          }
        }

      </Admin>
  );
}

const CustomLayout = props => {
  return <Layout {...props} menu={Menu} appBar={CustomAppBar}/>;
};

const CustomAppBar = props => {
  return <AppBar {...props} userMenu={<CustomUserMenu/>}/>
}

export default App;