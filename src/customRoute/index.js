import React from 'react';
import {Route} from 'react-router-dom';
import {Dashboard} from '../component/view/Dashboard';
import {ChangePassword} from '../component/view/ChangePassword';

export default [
  <Route exact path="/dashboard" component={Dashboard}/>,
  <Route exact path="/change-password" component={ChangePassword}/>
];