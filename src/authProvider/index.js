import decodeJwt from 'jwt-decode';
import _ from 'lodash';
import {BASE_URL} from '../appConstants';

const authProvider = {
  login: ({ username, password }) => {
    const request = new Request(`${BASE_URL}/auth/login`, {
      method: 'POST',
      body: JSON.stringify({ email: username, password }),
      headers: new Headers({ 'Content-Type': 'application/json' })
    });

    return fetch(request)
        .then(response => {
          if (response.status < 200 || response.status >= 300) {
            throw new Error(response.statusText);
          }
          return response.json();
        })
        .then(({ accessToken }) => {
          localStorage.setItem('token', accessToken);
          const roles = decodeJwt(accessToken).roles.map(role => {
            const words = _.words(role)
            const last = _.last(words)
            return _.lowerCase(last);
          });
          localStorage.setItem('permissions', JSON.stringify(roles));
        });
  },
  logout: () => {
    localStorage.removeItem('token');
    return Promise.resolve();
  },
  checkAuth: () => {
    if (!localStorage.getItem('token')) {
      return Promise.reject();
    }
    return Promise.resolve();
  },
  checkError: (error) => {
    const { status } = error;
    if (status === 401) {
      localStorage.removeItem('token');
      return Promise.reject();
    }
    return Promise.resolve();
  },
  getPermissions: () => {
    const roles = localStorage.getItem('permissions') ? JSON.parse(localStorage.getItem('permissions')) : null;
    return roles ? Promise.resolve(roles) : Promise.reject();
  },
  changePassword: ({previousPassword, newPassword}) => {
    const request = new Request(`${BASE_URL}/employees/change-password`, {
      method: 'PUT',
      body: JSON.stringify({previousPassword, newPassword}),
      headers: new Headers({'Authorization': `Bearer ${localStorage.getItem('token')}`})
    })

    return fetch(request)
        .then(response => {
          if (response.status < 200 || response.status >= 300) {
            throw new Error(response.statusText);
          }
          return response;
        })
  }
};

export default authProvider;