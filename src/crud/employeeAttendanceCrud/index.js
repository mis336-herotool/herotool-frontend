import React from 'react';
import {Create, DateField, DateInput, Edit, SelectField, SelectInput, Show, SimpleForm, SimpleShowLayout, TextField, TextInput, usePermissions} from 'react-admin';
import {Permission, permissionHandler} from '../../utils';
import {FullNameField} from '../../component/FullNameField';

export const EmployeeAttendanceCreate = (props) => {
  const { loading, permissions } = usePermissions();

  if (loading || (permissions && !permissionHandler(permissions, Permission.EMPLOYEE, true))) {
    return null;
  }

  return (
      <Create {...props}>
        <SimpleForm>
          <TextInput source="reason" multiline/>
          <DateInput source="startDate" label="Vacation start date"/>
          <DateInput source="endDate" label="Vacation end date"/>
        </SimpleForm>
      </Create>
  );
};

export const EmployeeAttendanceEdit = (props) => {
  const { permissions } = props;
  console.log(props);

  return (
      <Edit {...props}>
        <SimpleForm>
          <TextInput disabled label="Id" source="id"/>
          {permissionHandler(permissions, Permission.MANAGEMENT) ? <FullNameField nameSource="employee.name" surnameSource="employee.surname"/> : null}
          <TextInput disabled={!permissionHandler(permissions, Permission.EMPLOYEE, true)} source="reason" multiline/>
          <DateInput disabled={!permissionHandler(permissions, Permission.EMPLOYEE, true)} source="startDate" label="Vacation start date"/>
          <DateInput disabled={!permissionHandler(permissions, Permission.EMPLOYEE, true)} source="endDate" label="Vacation end date"/>
          {permissionHandler(permissions, Permission.MANAGEMENT) ? <SelectInput choices={[
            { id: 'PENDING', name: 'Pending', disabled: true },
            { id: 'ACCEPTED', name: 'Accepted' },
            { id: 'REJECTED', name: 'Rejected' }
          ]} source="status"/> : null}
        </SimpleForm>
      </Edit>
  );
};

export const EmployeeAttendanceShow = (props) => {
  const { loading, permissions } = usePermissions();

  if (loading) {
    return null;
  }

  return (
      <Show {...props}>
        <SimpleShowLayout>
          <TextField source="id"/>
          {permissionHandler(permissions, Permission.MANAGEMENT) ? <FullNameField nameSource="employee.name" surnameSource="employee.surname"/> : null}
          <TextField source="reason" multiline/>
          <DateField source="startDate" label="Start date"/>
          <DateField source="endDate" label="End date"/>
          <SelectField source="status" choices={[
            { id: 'PENDING', name: 'Pending' },
            { id: 'ACCEPTED', name: 'Accepted' },
            { id: 'REJECTED', name: 'Rejected' }
          ]}/>
        </SimpleShowLayout>
      </Show>
  );
};