import React from 'react';
import {Create, Edit, NumberInput, SimpleForm, TextInput} from 'react-admin';

export const BenefitCreate = (props) => (
    <Create {...props}>
      <SimpleForm>
        <TextInput source="name"/>
        <NumberInput source="value"/>
      </SimpleForm>
    </Create>);

export const BenefitEdit = (props) => (
    <Edit {...props}>
      <SimpleForm>
        <TextInput source="name"/>
        <NumberInput source="value"/>
      </SimpleForm>
    </Edit>
);