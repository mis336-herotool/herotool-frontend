import React from 'react';

import {
  ArrayField,
  Create,
  DateField,
  DateInput,
  Edit,
  email,
  EmailField,
  ReferenceField,
  ReferenceInput,
  required,
  SelectField,
  SelectInput,
  Show,
  SimpleForm,
  SimpleShowLayout,
  SingleFieldList,
  TextField,
  TextInput,
  usePermissions
} from 'react-admin';
import {Permission, permissionHandler} from '../../utils';
import {RolesField} from '../../component/RolesField';
import {RestrictedSelectArrayInput} from '../../component/RestrictedField';
import {makeStyles} from '@material-ui/core/styles';
import {FullNameField} from '../../component/FullNameField';

const validateEmail = [email(), required()];
const useStyles = makeStyles({
  inlineBlock: { display: 'inline-flex', marginRight: '1rem' }
});

export const EmployeeCreate = (props) => {
  const { loading: authLoading, permissions } = usePermissions();
  const classes = useStyles();

  if (authLoading || (permissions && !permissionHandler(permissions, Permission.HR))) {
    return null;
  }

  return (
      <Create {...props}>
        <SimpleForm>
          <TextInput formClassName={classes.inlineBlock} source="name" validate={required()}/>
          <TextInput formClassName={classes.inlineBlock} source="surname" validate={required()}/>
          <DateInput label="Birth date" source="birthDate" validate={required()}/>
          <RestrictedSelectArrayInput permission={permissionHandler(permissions, Permission.HR)} label="Roles" source="roles" choices={[
            { id: 'HR', name: 'HR' },
            { id: 'MANAGEMENT', name: 'Management' },
            { id: 'EMPLOYEE', name: 'Employee' }
          ]}/>
          <SelectInput source="title" choices={[
            { id: 'ASSISTANT', name: 'Assistant' },
            { id: 'CEO', name: 'CEO' },
            { id: 'CONSULTANT', name: 'Consultant' },
            { id: 'DIRECTOR', name: 'Director' },
            { id: 'EXECUTIVE_VICE_PRESIDENT', name: 'Executive Vice President' },
            { id: 'GROUP_MANAGER', name: 'Group Manager' },
            { id: 'JUNIOR_SPECIALIST', name: 'Junior Specialist' },
            { id: 'MANAGER', name: 'Manager' },
            { id: 'PRINCIPAL_SPECIALIST', name: 'Principal Specialist' },
            { id: 'SENIOR_SPECIALIST', name: 'Senior Specialist' },
            { id: 'SPECIALIST', name: 'Specialist' },
            { id: 'TEAM_LEADER', name: 'Team Leader' }
          ]} validate={required()}/>
          <TextInput source="position" validate={required()}/>
          <TextInput source="department" validate={required()}/>
          <TextInput source="phoneNumber" label="Phone number" validate={required()}/>
          <TextInput source="email" type="email" validate={validateEmail}/>
          <TextInput source="address" multiline resettable validate={required()}/>
          <DateInput label="Job start date" source="jobStartDate" validate={required()}/>
          <ReferenceInput reference="employees" source="manager" allowEmpty={true}>
            <SelectInput optionText={(employee) => (employee ? `${employee.name} ${employee.surname}` : null)}/>
          </ReferenceInput>
          <DateInput label="Job end date" source="jobEndDate"/>
          <SelectInput label="Marital status" source="maritalStatus" choices={[
            { id: 'MARRIED', name: 'Married' },
            { id: 'SINGLE', name: 'Single' },
            { id: 'WIDOWED', name: 'Widowed' },
            { id: 'DIVORCED', name: 'Divorced' }
          ]}/>
        </SimpleForm>
      </Create>
  );
};

export const EmployeeShow = (props) => {
  return (<Show {...props}>
    <SimpleShowLayout>
      <TextField disabled label="Id" source="id"/>
      <TextField source="name"/>
      <TextField source="surname"/>
      <DateField label="Birth date" source="birthDate"/>
      <ArrayField source="roles">
        <SingleFieldList linkType={false}>
          <RolesField/>
        </SingleFieldList>
      </ArrayField>
      <SelectField source="title" choices={[
        { id: 'ASSISTANT', name: 'Assistant' },
        { id: 'CEO', name: 'CEO' },
        { id: 'CONSULTANT', name: 'Consultant' },
        { id: 'DIRECTOR', name: 'Director' },
        { id: 'EXECUTIVE_VICE_PRESIDENT', name: 'Executive Vice President' },
        { id: 'GROUP_MANAGER', name: 'Group Manager' },
        { id: 'JUNIOR_SPECIALIST', name: 'Junior Specialist' },
        { id: 'MANAGER', name: 'Manager' },
        { id: 'PRINCIPAL_SPECIALIST', name: 'Principal Specialist' },
        { id: 'SENIOR_SPECIALIST', name: 'Senior Specialist' },
        { id: 'SPECIALIST', name: 'Specialist' },
        { id: 'TEAM_LEADER', name: 'Team Leader' }
      ]}/>
      <TextField source="position"/>
      <TextField source="phoneNumber" label="Phone number"/>
      <EmailField source="email" type="email"/>
      <TextField source="address" multiline/>
      <DateField label="Job start date" source="jobStartDate"/>
      <ReferenceField label="Manager" reference="employees" source="manager">
        <FullNameField isLabeled={false} nameSource="name" surnameSource="surname"/>
      </ReferenceField>
      <DateField label="Job end date" source="jobEndDate"/>
      <SelectField label="Marital status" source="maritalStatus" choices={[
        { id: 'MARRIED', name: 'Married' },
        { id: 'SINGLE', name: 'Single' },
        { id: 'WIDOWED', name: 'Widowed' },
        { id: 'DIVORCED', name: 'Divorced' }
      ]}/>
    </SimpleShowLayout>
  </Show>);
};

export const EmployeeEdit = (props) => {
  const { loading: authLoading, permissions } = usePermissions();
  const classes = useStyles();

  if (authLoading || (permissions && !permissionHandler(permissions, Permission.HR))) {
    return null;
  }

  return (<Edit {...props}>
    <SimpleForm>
      <TextInput formClassName={classes.inlineBlock} source="name" validate={required()}/>
      <TextInput formClassName={classes.inlineBlock} source="surname" validate={required()}/>
      <DateInput label="Birth date" source="birthDate" validate={required()}/>
      <RestrictedSelectArrayInput permission={permissionHandler(permissions, Permission.HR)} label="Roles" source="roles" choices={[
        { id: 'HR', name: 'HR' },
        { id: 'MANAGEMENT', name: 'Management' },
        { id: 'EMPLOYEE', name: 'Employee' }
      ]}/>
      <SelectInput source="title" choices={[
        { id: 'ASSISTANT', name: 'Assistant' },
        { id: 'CEO', name: 'CEO' },
        { id: 'CONSULTANT', name: 'Consultant' },
        { id: 'DIRECTOR', name: 'Director' },
        { id: 'EXECUTIVE_VICE_PRESIDENT', name: 'Executive Vice President' },
        { id: 'GROUP_MANAGER', name: 'Group Manager' },
        { id: 'JUNIOR_SPECIALIST', name: 'Junior Specialist' },
        { id: 'MANAGER', name: 'Manager' },
        { id: 'PRINCIPAL_SPECIALIST', name: 'Principal Specialist' },
        { id: 'SENIOR_SPECIALIST', name: 'Senior Specialist' },
        { id: 'SPECIALIST', name: 'Specialist' },
        { id: 'TEAM_LEADER', name: 'Team Leader' }
      ]}/>
      <TextInput source="position" validate={required()}/>
      <TextInput source="department" validate={required()}/>
      <TextInput source="phoneNumber" label="Phone number" validate={required()}/>
      <TextInput source="email" type="email" validate={validateEmail}/>
      <TextInput source="address" multiline resettable validate={required()}/>
      <ReferenceInput reference="employees" source="manager" allowEmpty={true}>
        <SelectInput optionText={(employee) => (employee ? `${employee.name} ${employee.surname}` : null)}/>
      </ReferenceInput>
      <DateInput label="Job start date" source="jobStartDate" validate={required()}/>
      <DateInput label="Job end date" source="jobEndDate"/>
      <SelectInput label="Marital status" source="maritalStatus" choices={[
        { id: 'MARRIED', name: 'Married' },
        { id: 'SINGLE', name: 'Single' },
        { id: 'WIDOWED', name: 'Widowed' },
        { id: 'DIVORCED', name: 'Divorced' }
      ]}/>
    </SimpleForm>
  </Edit>);
};