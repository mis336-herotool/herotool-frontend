import React from 'react';
import {Edit, NumberInput, ReferenceArrayInput, SelectArrayInput, SelectInput, SimpleForm, TextInput} from 'react-admin';
import {makeStyles} from '@material-ui/core/styles';

const useStyles = makeStyles({
  inlineBlock: { display: 'inline-flex', marginRight: '1rem' }
});

export const EmployeeBenefitEdit = (props) => {
  const classes = useStyles();

  return (
      <Edit {...props}>
        <SimpleForm>
          <TextInput disabled formClassName={classes.inlineBlock} source="name"/>
          <TextInput disabled formClassName={classes.inlineBlock} source="surname"/>
          <SelectInput disabled source="title" choices={[
            { id: 'ASSISTANT', name: 'Assistant' },
            { id: 'CEO', name: 'CEO' },
            { id: 'CONSULTANT', name: 'Consultant' },
            { id: 'DIRECTOR', name: 'Director' },
            { id: 'EXECUTIVE_VICE_PRESIDENT', name: 'Executive Vice President' },
            { id: 'GROUP_MANAGER', name: 'Group Manager' },
            { id: 'JUNIOR_SPECIALIST', name: 'Junior Specialist' },
            { id: 'MANAGER', name: 'Manager' },
            { id: 'PRINCIPAL_SPECIALIST', name: 'Principal Specialist' },
            { id: 'SENIOR_SPECIALIST', name: 'Senior Specialist' },
            { id: 'SPECIALIST', name: 'Specialist' },
            { id: 'TEAM_LEADER', name: 'Team Leader' }
          ]}/>
          <TextInput disabled source="position"/>
          <ReferenceArrayInput label="Benefits" source="benefits" reference="benefits">
            <SelectArrayInput optionText="name"/>
          </ReferenceArrayInput>
          <NumberInput min={50} step={0.01} source="salary"/>
        </SimpleForm>
      </Edit>
  );
};