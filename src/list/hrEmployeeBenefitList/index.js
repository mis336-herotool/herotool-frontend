import React from 'react';
import {ChipField, Datagrid, List, NumberField, ReferenceArrayField, SingleFieldList, TextField} from 'react-admin';

export const EmployeeBenefitList = (props) => (
    <List {...props}>
      <Datagrid rowClick="edit">
        <TextField source="id"/>
        <TextField source="name"/>
        <TextField source="surname"/>
        <TextField source="title"/>
        <TextField source="position"/>
        <TextField source="manager"/>
        <ReferenceArrayField label="Benefits" reference="benefits" source="benefits">
          <SingleFieldList>
            <ChipField source="name"/>
          </SingleFieldList>
        </ReferenceArrayField>
        <NumberField source="salary"/>
      </Datagrid>
    </List>
);