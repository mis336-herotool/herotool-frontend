import React from 'react';
import {Datagrid, List, TextField} from 'react-admin';

export const BenefitList = (props) => (
    <List {...props}>
      <Datagrid>
        <TextField source="id"/>
        <TextField source="name"/>
        <TextField source="value"/>
      </Datagrid>
    </List>
);