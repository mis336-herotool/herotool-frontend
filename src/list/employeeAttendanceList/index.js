import React from 'react';
import {Datagrid, DateField, Filter, List, SelectField, TextField, TextInput, usePermissions} from 'react-admin';
import {Permission, permissionHandler} from '../../utils';
import {Chip, makeStyles} from '@material-ui/core';
import {FullNameField} from '../../component/FullNameField';

export const EmployeeAttendanceList = (props) => {
  const {loading, permissions} = usePermissions();

  if (loading) return (<div>Loading...</div>);

  return (
      <List {...props} filters={<EmployeeAttendanceFilter/>}>
        <Datagrid rowClick={permissionHandler(permissions, Permission.MANAGEMENT) ? 'edit' : 'show'}>
          <TextField source="id"/>
          {permissionHandler(permissions, Permission.MANAGEMENT) ? <FullNameField isLabeled={false} nameSource="employee.name" surnameSource="employee.surname"/> : null}
          <TextField source="reason" multiline/>
          <DateField source="startDate" label="Start date"/>
          <DateField source="endDate" label="End date"/>
          <SelectField source="status" choices={[
            {id: 'PENDING', name: 'Pending'},
            {id: 'ACCEPTED', name: 'Accepted'},
            {id: 'REJECTED', name: 'Rejected'},
          ]}/>
        </Datagrid>
      </List>
  );
};

export const EmployeeAttendanceFilter = (props) => (
    <Filter {...props}>
      <TextInput label="Search" source="reason" alwaysOn/>
      <QuickFilter label="Pending" source="status" defaultValue="PENDING"/>
    </Filter>
);

const useQuickFilterStyles = makeStyles(theme => ({
  chip: {
    marginBottom: theme.spacing(1),
  },
}));

export const QuickFilter = ({label}) => {
  const classes = useQuickFilterStyles();
  return <Chip className={classes.chip} label={label}/>;
};