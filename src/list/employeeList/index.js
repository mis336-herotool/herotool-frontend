import React from 'react';
import {Datagrid, DateField, EmailField, List, ReferenceField, SelectField, TextField} from 'react-admin';
import {FullNameField} from '../../component/FullNameField';

export const EmployeeList = (props) => (
    <List {...props}>
      <Datagrid rowClick="show">
        <TextField source="id"/>
        <TextField source="name"/>
        <TextField source="surname"/>
        <DateField source="birthDate"/>
        <TextField source="title"/>
        <TextField source="position"/>
        <TextField source="phoneNumber"/>
        <EmailField source="email"/>
        <TextField source="address"/>
        <DateField source="jobStartDate"/>
        <ReferenceField label="Manager" reference="employees" source="manager">
          <FullNameField isLabeled={false} nameSource="name" surnameSource="surname"/>
        </ReferenceField>
        <TextField source="jobEndDate"/>
        <SelectField label="Marital Status" source="maritalStatus" choices={[
          {id: 'MARRIED', name: 'Married'},
          {id: 'SINGLE', name: 'Single'},
          {id: 'WIDOWED', name: 'Widowed'},
          {id: 'DIVORCED', name: 'Divorced'},
        ]}/>
      </Datagrid>
    </List>
);