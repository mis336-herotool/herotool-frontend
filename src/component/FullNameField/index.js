import React from 'react';
import {Labeled} from 'react-admin';
import {Typography} from '@material-ui/core';
import get from 'lodash/get';

export const FullNameField = ({ record = {}, label, isLabeled = true, nameSource, surnameSource, ...rest }) => {
  if (isLabeled) {
    return <Labeled label={label}><span {...rest}>{get(record, nameSource)} {get(record, surnameSource)}</span></Labeled>;
  } else {
    return <Typography {...rest}>{get(record, nameSource)} {get(record, surnameSource)}</Typography>;
  }
};

FullNameField.defaultProps = { label: 'Name' };

