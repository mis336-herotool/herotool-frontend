import React from 'react';
import {makeStyles} from '@material-ui/styles';
import {Grid} from '@material-ui/core';
import PropTypes from 'prop-types';
import classNames from 'classnames'

const useStyles = makeStyles({
  grid: {
    margin: '0 -15px !important',
    width: '100%'
  },
});

export default function GridContainer(props) {
  const classes = useStyles();
  const {children, className, ...rest} = props;
  return (
      <Grid container {...rest} className={classNames({
        [classes.grid]: true,
        [className]: className !== undefined
      })}>
        {children}
      </Grid>
  );
}

GridContainer.propTypes = {
  children: PropTypes.node,
};



