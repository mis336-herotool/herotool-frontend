import React from 'react';
import {Grid} from '@material-ui/core';
import {makeStyles} from '@material-ui/styles';
import PropTypes from 'prop-types';

const useStyles = makeStyles({
  gridItem: {
    padding: '0 15px !important',
  },
});

export default function GridItem(props) {
  const classes = useStyles();
  const {children, ...rest} = props;
  return (
      <Grid item {...rest} className={classes.gridItem}>
        {children}
      </Grid>
  )
}

GridItem.propTypes = {
  children: PropTypes.node
}