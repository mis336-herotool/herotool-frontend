import {SelectArrayInput} from 'react-admin';
import React from 'react';

export const RestrictedSelectArrayInput = ({ permission, children, ...rest }) => (permission ? <SelectArrayInput {...rest}/> : null);