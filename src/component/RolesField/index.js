import React from 'react';
import {Chip, makeStyles} from '@material-ui/core';

const useChipStyles = makeStyles({
  chip: {}
});

export const RolesField = ({ record = {} }) => {
  const classes = useChipStyles();
  return <Chip className={classes.chip} label={record}/>;
};