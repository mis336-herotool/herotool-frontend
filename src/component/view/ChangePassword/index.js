import React from 'react';
import {useMutation} from 'react-admin';
import {useHistory} from 'react-router-dom';
import {Field, Form} from 'react-final-form';
import {Alert} from '@material-ui/lab'

export const ChangePassword = (props) => {
  const history = useHistory();
  const [mutate, {loading, error}] = useMutation();

  const onSubmit = values => {
    mutate({
      type: 'changePassword',
      payload: {values},
    }, {onSuccess: () => {history.push('/dashboard')}});
  };

  return ([
        <Form onSubmit={onSubmit}
              render={({handleSubmit, form, submitting, pristine, values}) => {
                return (
                    <form onSubmit={handleSubmit}>
                      <div>
                        <label>Password</label>
                        <Field name="password"
                               component="input"
                               type="password"
                               placeholder="Password"/>
                      </div>
                      <div>
                        <label>New password</label>
                        <Field name="newPassword"
                               component="input"
                               type="password"
                               placeholder="Password"/>
                      </div>
                      <button type="submit" disabled={submitting || pristine || loading}>Submit</button>
                    </form>
                );
              }}
        />, error ? <Alert severity="error">Wrong password</Alert> : null,
      ]

  );
};