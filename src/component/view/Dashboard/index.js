import React from 'react';
import {Button, Card, CardActions, CardContent, Typography} from '@material-ui/core';
import ChartistGraph from 'react-chartist';
import {usePermissions, useQuery} from 'react-admin';
import {useHistory} from 'react-router-dom';
import {Permission, permissionHandler} from '../../../utils';
import GridContainer from '../../Grid/GridContainer';
import GridItem from '../../Grid/GridContainer';
import {makeStyles} from '@material-ui/core/styles';
import Chartist from 'chartist';
import '../../../assets/css/main.css';

const styles = makeStyles({
  gridContainer: {
    marginBottom: '15px',
  },
  card: {
    border: '0',
    marginBottom: '30px',
    marginTop: '30px',
    borderRadius: '6px',
    color: 'rgba(0,0,0, 0.87)',
    background: '#FFFFFF',
    width: '100%',
    boxShadow: '0 1px 4px 0 rgba(0,0,0, 0.14)',
    position: 'relative',
    display: 'flex',
    flexDirection: 'column',
    minWidth: '0',
    wordWrap: 'break-word',
    fontSize: '.875rem',
  },
  chartContainer: {
    background: '#3d5afe',
    boxShadow: '0 4px 20px 0 rgba(0, 0, 0,.14), 0 7px 10px -5px rgba(76, 175, 80,.4)',
    padding: '15px',
    borderRadius: '3px',
  },

});

export const Dashboard = props => {
  const {loading, permissions} = usePermissions();
  const classes = styles();
  const {data: employee, loading: queryLoading} = useQuery({type: 'getEmployeeProfile'});
  let components = [];

  if (loading || queryLoading)
    return <div/>;

  if (permissionHandler(permissions, Permission.HR, true)) {
    components.push();
  }

  if (permissionHandler(permissions, Permission.MANAGEMENT, true)) {
    components.push(
        <GridContainer className={classes.gridContainer}>
          <GridItem xs={12} sm={6} md={3}>
            <ManagementAttendanceRequestsCard/>
          </GridItem>
        </GridContainer>,
    );
  }

  if (permissionHandler(permissions, Permission.EMPLOYEE, true)) {
    components.push(
        <GridContainer className={classes.gridContainer} spacing={1}>
          <GridItem xs={12} sm={6} md={3}>
            <EmployeeInformationCard employee={employee} {...props} />
          </GridItem>
          <GridItem xs={12} sm={6} md={3}>
            <EmployeeSalaryCard {...props}/>
          </GridItem>
          <GridItem xs={12} sm={6} md={3}>
            <EmployeeHistoricalCard {...props}/>
          </GridItem>
          <GridItem xs={12} sm={6} md={3}>
            <EmployeeBenefitCard {...props}/>
          </GridItem>
        </GridContainer>,
    );
  }

  return (
      <div>
        <GridContainer>
          {components}
        </GridContainer>
      </div>
  );
};

const ManagementAttendanceRequestsCard = (props) => {
  const {data} = useQuery({type: 'getPendingAttendance'});
  const history = useHistory();

  return (
      <Card>
        <CardContent>
          <Typography variant="caption">Pending Vacation Requests</Typography>
          <Typography variant="h4">{data ? data.length : 0}</Typography>
        </CardContent>
        <CardActions>
          <Button onClick={() => {
            history.push('/attendance');
          }}>See more</Button>
        </CardActions>
      </Card>
  );
};

const EmployeeInformationCard = ({employee}) => {
  const classes = styles();
  return (
      <Card className={classes.card}>
        <CardContent>
          <Typography variant="caption">Department</Typography>
          <Typography variant="h6">{employee ? employee.department : null}</Typography>

          <Typography variant="caption">Title</Typography>
          <Typography variant="h6">{employee ? employee.title : null}</Typography>
          {
            employee && employee.manager ?
                ([
                  <Typography variant="caption">Manager</Typography>,
                  <Typography variant="h6">{employee ? employee.manager : null}</Typography>,
                ]) : null
          }
        </CardContent>
      </Card>
  );
};

const EmployeeSalaryCard = (props) => {
  const classes = styles();
  const {loading, data} = useQuery({type: 'getBenefitProfile'});

  if (loading) return null;

  return (
      <Card className={classes.card}>
        <CardContent>
          <Typography variant="caption">Salary</Typography>
          <Typography variant="h6">{data.activeSalary ? data.activeSalary + '$' : '0$'}</Typography>
          <Typography variant="caption">Department Average</Typography>
          <Typography variant="h6">{data.departmentAverageSalary + '$'}</Typography>
        </CardContent>
      </Card>
  );
};

const EmployeeHistoricalCard = (props) => {
  const classes = styles();
  const {loading, data} = useQuery({type: 'getBenefitProfile'});

  if (loading) return null;

  const chartData = {
    labels: data.salaryHistory.map(salary => `${salary.startDate[2]}/${salary.startDate[1]}/${salary.startDate[0]}`),
    series: [data.salaryHistory.map(salary => salary.salary)],
  };

  const options = {
    lineSmooth: Chartist.Interpolation.cardinal({
      tension: 0,
    }),
    chartPadding: {
      top: 0,
      right: 0,
      bottom: 0,
      left: 0,
    },
  };

  const animation = {
    draw: function(data) {
      if (data.type === 'line' || data.type === 'area') {
        data.element.animate({
          d: {
            begin: 600,
            dur: 700,
            from: data.path.clone().scale(1, 0).translate(0, data.chartRect.height()).stringify(),
            to: data.path.clone().stringify(),
            easing: Chartist.Svg.Easing.easeOutQuint,
          },
        });
      } else if (data.type === 'point') {
        data.element.animate({
          opacity: {
            begin: (data.index + 1) * 80,
            dur: 500,
            from: 0,
            to: 1,
            easing: 'ease',
          },
        });
      }
    },
  };

  return (
      <Card className={classes.card}>
        <CardContent>
          <Typography variant="caption">Salary History</Typography>
          <div className={classes.chartContainer}>
            <ChartistGraph className="ct-chart"
                           data={chartData}
                           type="Line"
                           options={options}
                           listener={animation}/>
          </div>
        </CardContent>
      </Card>
  );
};

const EmployeeBenefitCard = (props) => {
  const {loading, data} = useQuery({type: 'getBenefitProfile'});
  const classes = styles();

  if (loading) return null;

  return (
      <Card className={classes.card}>
        <CardContent>
          <Typography variant="caption">Benefits</Typography>
          {data.benefits.map(benefit => <Typography variant="h6">{benefit.name}</Typography>)}
        </CardContent>
      </Card>
  );
};