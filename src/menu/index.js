import React from 'react';
import {useSelector} from 'react-redux';
import {useMediaQuery} from '@material-ui/core';
import {getResources, MenuItemLink, UserMenu} from 'react-admin';
import LabelIcon from '@material-ui/icons/Label';
import SettingsIcon from '@material-ui/icons/Settings';
import DefaultIcon from '@material-ui/icons/ViewList';

export const Menu = ({onMenuClick, logout, ...rest}) => {
  const open = useSelector(state => state.admin.ui.sidebarOpen);
  const resources = useSelector(getResources);
  const isXSmall = useMediaQuery(theme => theme.breakpoints.down('xs'));
  return (
      <div>
        <MenuItemLink
            to="/dashboard"
            primaryText="Home"
            leftIcon={<LabelIcon/>}
            onClick={onMenuClick}
            sidebarIsOpen={open}
        />
        {resources.map(resource =>
            <MenuItemLink
                key={resource.name}
                to={`/${resource.name}`}
                primaryText={(resource.options && resource.options.label) || resource.name}
                leftIcon={resource.icon ? <resource.icon/> : <DefaultIcon/>}
                onClick={onMenuClick}
                sidebarIsOpen={open}
            />,
        )}
        {isXSmall && logout}
      </div>
  );
};

export const CustomUserMenu = props => {
  return (
      <UserMenu {...props}>
        <MenuItemLink
            to="/change-password"
            primaryText="Change Password"
            leftIcon={<SettingsIcon/>}/>
      </UserMenu>);
};