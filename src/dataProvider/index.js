import {fetchUtils} from 'react-admin';
import qs from 'query-string';
import {BASE_URL} from '../appConstants';

const restClient = (url, options = {}) => {
  if (!options.headers) {
    options.headers = new Headers({'Content-Type': 'application/json'});
  }

  const token = localStorage.getItem('token');
  options.headers.set('Authorization', `Bearer ${token}`);

  return fetchUtils.fetchJson(`${options.queryParams ? url.concat('?' + qs.stringify(options.queryParams)) : url}`, options);
};

export default {
  getList: (resource, params) => {
    const {page = 0, perPage = 25} = params.pagination;
    const {field, order = 'ASC'} = params.sort;

    let queryParams = {page: page - 1, size: perPage};
    if (field) {
      queryParams = {...queryParams, sort: `${field},${order}`};
    }
    if (params.filter) {
      queryParams = {...queryParams, ...params.filter};
    }

    return restClient(`${BASE_URL}/${resource}`, {method: 'GET', queryParams: queryParams}).
        then(res => resolveHttpResponse(res, (data) => (data['_embedded'] ? Object.values(data['_embedded'])[0] : []), true));
  },
  getAll: (resource, params) => {
    return restClient(`${BASE_URL}/${resource}/all`, {method: 'GET'}).then(res => resolveHttpResponse(res));
  },
  getOne: (resource, params) => {
    const {id} = params;
    if (!id) {
      throw new Error('The id property must be present in the request.');
    }

    return restClient(`${BASE_URL}/${resource}/${id}`, {method: 'GET'}).then(res => resolveHttpResponse(res));
  },
  getMany: (resource, params) => {
    const {ids} = params;

    return Promise.all(ids.map(id => restClient(`${BASE_URL}/${resource}/${id}`, {method: 'GET'}))).then(responses => ({data: responses.map(response => response.json)}));
  },
  getManyReference: (resource, params) => {
    const {page = 0, perPage = 25} = params.pagination;
    const {field, order = 'ASC'} = params.sort;

    let queryParams = {page: page - 1, size: perPage};
    if (field) {
      queryParams = {...queryParams, sort: `${field},${order}`};
    }
    if (params.filter) {
      queryParams = {...queryParams, ...params.filter};
    }

    return restClient(`${BASE_URL}/${resource}`, {method: 'GET', queryParams: queryParams}).
        then(res => resolveHttpResponse(res, (data) => (data['_embedded'] ? Object.values(data['_embedded'])[0] : []), true));
  },
  create: (resource, params) => {
    const {data} = params;

    return restClient(`${BASE_URL}/${resource}`, {method: 'POST', body: JSON.stringify(data)}).then(res => resolveHttpResponse(res));
  },
  update: (resource, params) => {
    const {id, data} = params;

    return restClient(`${BASE_URL}/${resource}/${id}`, {method: 'PUT', body: JSON.stringify(data)}).then(res => resolveHttpResponse(res));
  },
  updateMany: (resource, params) => {
    const {ids, data} = params;

    return Promise.all(ids.map(id => restClient(`${BASE_URL}/${resource}/${id}`, {method: 'PUT', body: data}))).then(responses => ({data: responses.map(res => resolveHttpResponse(res))}));
  },
  delete: (resource, params) => {
    const {id} = params;

    return restClient(`${BASE_URL}/${resource}/${id}`, {method: 'DELETE'}).then(res => resolveHttpResponse(res));
  },
  deleteMany: (resource, params) => {
    const {ids} = params;

    return Promise.all(ids.map(id => restClient(`${BASE_URL}/${resource}/${id}`, {method: 'DELETE'}))).then(responses => ({data: responses.map(res => resolveHttpResponse(res))}));
  },
  getEmployeeProfile: () => {
    return restClient(`${BASE_URL}/employees/me`).then(res => ({data: res.json}));
  },
  getBenefitProfile: () => {
    return restClient(`${BASE_URL}/benefits/me`).then(res => ({data: res.json}));
  },
  getPendingAttendance: () => {
    return restClient(`${BASE_URL}/attendance/pending`).then(res => ({data: res.json}))
  },
  changePassword: (resource, params) => {
    const {password, newPassword} = params.values;
    return restClient(`${BASE_URL}/employees/change-password`, {method: 'PUT', body: JSON.stringify({previousPassword: password, newPassword})}).then(res => ({data: res.json}))
  }
};

const resolveHttpResponse = (response, dataAccessor = null, pageable = false) => {
  const {json} = response;
  const res = {'data': dataAccessor ? dataAccessor(json) : json};

  if (pageable) {
    if (!json.hasOwnProperty('page')) {
      throw new Error('The numberOfElements property must be present in the Json response');
    }
    res['total'] = json.page.totalElements;
  }

  return res;
};