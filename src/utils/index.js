export const permissionHandler = (permissions, requirement = Permission.EMPLOYEE, exact = false) => {
  if (!permissions && !Array.isArray(permissions)) {
    return false;
  } else {

    // Superuser
    if (permissions.includes(Permission.ADMIN.value)) {
      return true;
    }

    // Must contain requirement
    if (exact) {
      return permissions.includes(requirement.value);
    } else {
      if (permissions.includes(Permission.HR.value)) {
        return requirement.priority >= Permission.HR.priority;
      }
      if (permissions.includes(Permission.MANAGEMENT.value)) {
        return requirement.priority >= Permission.MANAGEMENT.priority;
      }
      if (permissions.includes(Permission.EMPLOYEE.value)) {
        return requirement.priority >= Permission.EMPLOYEE.priority;
      }
    }

    return false;
  }
};

export const Permission = {
  ADMIN: {priority: 1, value: 'admin'},
  HR: {priority: 2, value: 'hr'},
  MANAGEMENT: {priority: 3, value: 'management'},
  EMPLOYEE: {priority: 4, value: 'employee'},
};