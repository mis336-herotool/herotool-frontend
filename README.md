#HeroTool Frontend

###How to install

####Environment Parameters
* `REACT_APP_BASE_URL` Base URL for HeroTool Backend application. eg `http://localhost:8080`

###How to run
First dependencies must be installed with `npm install`
After that application can be built with `npm build` and server can be started with `npm start`

Or after dependencies installed, application can be started with `npm start:dev` in developer mode

