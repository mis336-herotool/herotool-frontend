FROM node:12 as build-env
COPY . /src
WORKDIR src
RUN npm install

FROM node:12 as runtime
COPY --from=build-env src/ src/
WORKDIR src
CMD ["npm", "start"]
EXPOSE 4000